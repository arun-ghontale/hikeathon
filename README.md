# Hikeathon

solution for kaggle competetion -> https://datahack.analyticsvidhya.com/contest/hikeathon/

## Data Description
* The data for this competition is a subset of the Hike’s social graph and the anonymised features of users. Explicitly, the training data is of the following form:
* train.zip contains 2 files namely train.csv, user_features.csv

## train.csv
* node1_id, node2_id, is_chat
* Where node1_id and node2_id are anonymised identifiers for users who are in each other’s phone address book. is_chat signifies their chat relationship. is_chat is 1, if the first user sends a chat message with the second user, and 0 otherwise.

## user_features.csv
* node_id, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13
* This file contains some anonymised features for all nodes/users. Here node_id (corresponding to node1_id and node2_id in train/test files) represents the user for whom we have features from f1 to f13

## test.csv (contained in test.zip)
* Build a model that can learn to predict probability of a node-pair in the test set to have a chat relation. The test set contains an id and a pairs of nodes id, node1_id, node2_id
* for which participants are required to predict is_chat on the test set. (Note that id is just here to identify a unique row in test set and is used in the submission format section)